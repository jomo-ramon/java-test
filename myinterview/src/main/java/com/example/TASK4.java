package com.example;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import com.google.gson.Gson;

/**
 * Create an implementation of a Rest API client.
 * Prints out how many records exists for each gender and save this file to s3 bucket
 * API endpoint=> https://3ospphrepc.execute-api.us-west-2.amazonaws.com/prod/RDSLambda 
 * AWS s3 bucket => interview-digiage
 *
 */
public class TASK4 {
	public static void main(String[] args) throws MalformedURLException, IOException {
	    
        String url = "https://3ospphrepc.execute-api.us-west-2.amazonaws.com/prod/RDSLambda";

        HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();

        conn.setRequestMethod("GET");
        conn.setRequestProperty("Accept", "application/json");

       if(conn.getResponseCode() != 200) {
    	   System.out.println(conn.getResponseCode());
       }else {
    	   System.out.println("Conectou");
       }
       BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
       
      System.out.println(br);
      String output = "";
      String line;
      while ((line = br.readLine()) != null) {
          output += line;
      }
      conn.disconnect();

      Gson gson = new Gson();
      
      String dados = gson.toJson(new String(output.getBytes()));
      
      System.out.println(dados);
      // apartir daqui não consegui tratar o json.
	}
}