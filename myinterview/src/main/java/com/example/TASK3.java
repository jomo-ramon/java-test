package com.example;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

/*
 * Write a list and add an aleatory number of Strings. In the end, print out how
 * many distinct itens exists on the list.
 *
 */


public class TASK3 {

	public static void main(String[] args) {
	    // declaring a new list
	    ArrayList<Integer> list = new ArrayList<Integer>();
	    // using the add method to write to the list
	    list.add(1);
	    list.add(1);
	    list.add(2);
	    list.add(3);
	    list.add(2);
	    list.add(3);
	    list.add(4);
	    list.add(4);
	    list.add(5);

	    final Map<Integer, Integer> quantity = new HashMap<Integer, Integer>();

	    quantity.put(1, Collections.frequency(list, 1));
	    quantity.put(2, Collections.frequency(list, 2));
	    quantity.put(3, Collections.frequency(list, 3));
	    quantity.put(4, Collections.frequency(list, 4));
	    quantity.put(5, Collections.frequency(list, 5));
	    
	    final String format = "Number: %d it has: %dº repetitions ";
	    final Set<Integer> numbers = quantity.keySet(); 
	    for (final Integer number : numbers) {
	        System.out.println(String.format(format, number, quantity.get(number)));
	    }

	  }
}
